package service

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/ericoliveiras/desafio-avanti-docker/app/models"
)

func GetUser(username string) (models.User, error) {
	url := fmt.Sprintf("https://api.github.com/users/%s", username)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return models.User{}, err
	}

	req.Header.Set("Accept", "application/vnd.github.v3+json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return models.User{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return models.User{}, fmt.Errorf("erro ao obter perfil do usuário: %s", resp.Status)
	}

	var user models.User
	if err := json.NewDecoder(resp.Body).Decode(&user); err != nil {
		return models.User{}, err
	}

	return user, nil
}
