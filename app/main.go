package main

import (
	"net/http"

	"gitlab.com/ericoliveiras/desafio-avanti-docker/app/controller"
)

func main() {
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("app/templates/css"))))
	http.HandleFunc("/", controller.SearchUser)
	http.ListenAndServe(":8080", nil)
}
