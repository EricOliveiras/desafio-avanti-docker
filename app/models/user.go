package models

type User struct {
	Login       string `json:"login"`
	AvatarURL   string `json:"avatar_url"`
	Followers   int    `json:"followers"`
	PublicRepos int    `json:"public_repos"`
}

type PageData struct {
	User *User
}
