# Use a imagem base do Golang
FROM golang:latest

# Define o diretório de trabalho dentro do container
WORKDIR /go/src/app

# Copia o arquivo go.mod do diretório raiz do projeto para o diretório de trabalho do container
COPY ../go.mod .

# Copia todo o código fonte do diretório raiz do projeto para o diretório de trabalho do container
COPY .. .

# Baixa as dependências
RUN go mod download

# Compila o aplicativo
RUN go build -o /app app/main.go

# Expoe a porta 8080 para o exterior do container
EXPOSE 8080

# Comando para executar o aplicativo quando o contêiner for iniciado
CMD [ "/app" ]
