# Desafio Avanti Docker

Este projeto tem como objetivo colocar em prática algumas práticas de DevOps, utilizando Terraform, AWS, GitLab CI/CD e Docker. Neste projeto, desenvolvi uma aplicação em Golang, HTML e CSS, cujo propósito é fornecer um serviço de busca de usuários do GitHub.

> Link do Deploy: http://3.91.46.225:8080/